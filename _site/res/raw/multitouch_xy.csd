<CsoundSynthesizer>
<CsOptions>
-o dac -d -b512 -B2048
</CsOptions>
<CsInstruments>
nchnls=2
0dbfs=1
ksmps=32
sr = 44100

ga1 init 0

instr 1

itie tival
i_instanceNum = p4
S_xName sprintf "touch.%d.x", i_instanceNum
S_yName sprintf "touch.%d.y", i_instanceNum

kx chnget S_xName
ky chnget S_yName

kenv linsegr 0, .001, 1, .1, 1, .25, 0

kcps = 1 + kx*500 
kcar = 1
kmod = 1 + ky*20 
kndx line 1, 30, 11	;intensivity sidebands

kamp = 100
itype = 4
;kcps = 5
klfo lfo kamp, 1 + kx*10, itype
a1 foscili .38, kcps + klfo + ky, kcar, kmod, kndx, 1

ga1 = ga1 + a1

endin

instr 2

kcutoff = 1060 
kresonance = 0.6


a1 moogladder ga1, kcutoff, kresonance

;aL, aR reverbsc a1, a1, .82, 50000

outs a1, a1

ga1 = 0

endin


</CsInstruments>
<CsScore>
f1 0 16384 10 1

i2 0 360000
 
</CsScore>
</CsoundSynthesizer>

